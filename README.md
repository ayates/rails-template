# Initforthe - rails-template

### This is the beginning of all Initforthe Ruby on Rails projects.

It will create a new Rails 7 project.

**Template Name:** Project Startup Template
**Author:** Initforthe LTD
**Author URI:** https://initforthe.com
**Instructions:**
> `$ rails new myapp -d <postgresql, mysql, sqlite> -m https://gitlab.com/initforthe/rails-template/raw/main/template.rb`

### Prerequisites

You'll need rbenv and the latest stable Ruby (currently 3.1) installed:

```
$ brew install rbenv
$ rbenv init
$ rbenv install 3.1
$ rbenv local 3.1
```

You'll need some gems installed:
```
$ gem install rails
```

If you are intending on using postgresql, you'll need `pg_config` installed to intall the pg gem.

On macOS:
```
$ brew install postgresql
```

On Ubuntu:
```
$ sudo apt install postgresql-client
```

### Installation
#### Optional.

To make this the default Rails application template on your system, **create a ~/.railsrc** file with these contents:
```
  -T
  -d postgresql
  -m https://gitlab.com/initforthe/rails-template/raw/main/template.rb
```

## Reference
* https://medium.com/today-i-learned-chai/setting-up-eslint-standard-prettier-babel-for-my-hapi-js-starter-project-4b8545e0162b
* https://www.futurehosting.com/blog/prettier-vs-eslint-whats-the-difference/
* https://github.com/RailsApps/rails-composer
* https://github.com/justalever/kickoff_tailwind
