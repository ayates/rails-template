# frozen_string_literal: true

require 'open-uri'
require 'debug' if ENV['LOCAL'] || ENV['DEBUG']

def command?(command)
  system("which #{command} > /dev/null 2>&1")
end

def get_path(source)
  path = if ENV['LOCAL']
           __dir__
         elsif ENV['BASE_PATH']
           ENV['BASE_PATH']
         else
           'https://gitlab.com/initforthe/rails-template/raw/main'
         end
  "#{path}/#{source}"
end

def remote_template(source, destination = source, config = {})
  source = get_path("templates/#{source}")
  context = instance_eval('binding', __FILE__, __LINE__)

  create_file destination, nil, config do
    # This needs to be URI.open to open remote files
    ERB.new(URI.open(source).read, trim_mode: '-').result(context) # rubocop:disable Security/Open
  end
end

def recipe(source)
  apply get_path("recipes/#{source}.rb")
end

def available_options(options)
  return nil if options.blank?

  set_color(options.join(', '), :yellow)
end

def format_options(options, prefix = 'Available options: ')
  return nil if options.blank?

  "#{prefix} #{options}"
end

def present_options(options)
  return nil if options.blank?

  say(format_options(options, 'Your answer needs to be one of:'))
end

def format_question(question, color = :cyan, default = nil, options = nil)
  default.present? &&
    default_opt = set_color("(default: #{default})", :yellow)

  available_options = available_options(options)

  [
    set_color(question, (color || :cyan)),
    format_options(available_options),
    default_opt
  ].compact.join(' ')
end

def validate_question(value, options)
  valid = true
  if options[:force] && !value.present?
    say('You need to supply an answer', :red)
    valid = false
  elsif options[:options] && value.present? &&
        !options[:options].include?(value)
    say('Your answer needs to be one of the options provider', :red)
    valid = false
  end

  valid
end

def prompt(question, options = {})
  formatted_question = format_question(
    question, options[:color], options[:default], options[:options]
  )
  answer = ask(formatted_question)
  value = answer.present? ? answer : options[:default]

  return prompt(question, options) unless validate_question(value, options)

  value
end

def subprompt(question, options = {})
  question_string = "** #{question}"
  if options[:if]
    prompt(question_string, options) if options[:if]
  elsif options[:unless]
    prompt(question_string, options) unless options[:unless]
  end
end

def default_yes?(question, options = {})
  question = set_color(question, options[:color] || :cyan)
  defaults = set_color('(Y/n)', :yellow)
  !no?([question, defaults].join(' '))
end

def default_no?(question, options = {})
  question = set_color(question, options[:color] || :cyan)
  defaults = set_color('(y/N)', :yellow)
  yes?([question, defaults].join(' '))
end

@configured_options = builder.options

# Ruby and Node versions
current_ruby_version = RUBY_VERSION
current_version_major = current_ruby_version.split('.')[0]
current_version_minor = current_ruby_version.split('.')[1]
@ruby_version = prompt('Ruby version:', default: "#{current_version_major}.#{current_version_minor}")
@ruby_builder = @ruby_version.gsub('.', '-')
@ruby_variant = @ruby_version.split('.').first

@node_version = prompt('Node.js version:', default: 'lts/*') unless @configured_options.api

# Authentication
@devise = default_yes?('Use Devise for user Authentication?')
@authentication_model = subprompt('Default User Authentication Model',
                                  default: 'User', if: @devise)

@users = default_yes?('Will your app have users?') unless @devise

# External services
@sentry = default_yes?('Use Sentry?')
@postmark = default_yes?('Use Postmark?')
@sentry_dsn = subprompt('Sentry DSN:', if: @sentry, force: true)
@git_repo = prompt('Git repository:', force: true)

pre_bundle_recipes = %w[git setup gemfile]

after_bundle_recipes = %w[linters rails_defaults settings tests deploy]
after_bundle_recipes << 'sentry' if @sentry
after_bundle_recipes << 'devise' if @devise
after_bundle_recipes += %w[icons turbo stimulusjs action_text view_component tailwindcss] unless @configured_options.api
after_bundle_recipes += %w[active_storage vscode binstubs postinstall pre_commit cleanup post_git messages]

pre_bundle_recipes.each { |r| recipe r }

after_bundle do
  after_bundle_recipes.each { |r| recipe r }
  run "#{ENV['EDITOR']} ." if ENV['EDITOR'] && default_yes?("Open in your default editor (#{ENV['EDITOR']})?")
end
