# frozen_string_literal: true

@setup_database = default_yes?('Set up database?') if ENV['REMOTE_CONTAINERS']

rails_command 'db:create db:migrate' if @setup_database
