# frozen_string_literal: true

gsub_file 'Gemfile', /gem\s+["']sass-rails["'].*$/, "gem 'sassc-rails'"
gsub_file 'Gemfile', /ruby\s+["']([0-9.]+)["']/, 'ruby \'~> \1\''
gsub_file 'Gemfile', /# gem\s+["']image_processing["'].*$/, "gem 'image_processing'"
gsub_file 'Gemfile', /# gem\s+["']bcrypt["'].*$/, "gem 'bcrypt'"

inject_into_file 'Gemfile', "git_source(:gitlab) { |repo| \"https://gitlab.com/\#{repo}.git\" }\n",
                 after: /git_source(.*)\n/

gem 'active_link_to'
gem 'config'
gem 'country_select'
gem 'crudable', gitlab: 'initforthe/crudable'
if @devise
  gem 'devise'
  gem 'devise-i18n'
  gem 'devise_invitable'
end
gem 'draper'
gem 'font_awesome5_rails'
gem 'has_scope'
gem 'heroicon', github: 'bharget/heroicon'
gem 'kaminari'
gem 'page_title_helper'
gem 'postmark-rails' if @postmark
gem 'pundit'
gem 'rails-i18n'
if @sentry
  gem 'sentry-rails'
  gem 'sentry-ruby'
  gem 'sentry-sidekiq'
end
gem 'sidekiq'
gem 'strip_attributes'
gem 'tailwindcss-rails'
gem 'view_component'

gem_group :development do
  gem 'annotate'
end

gem_group :test do
  gem 'capybara-email'
  gem 'cuprite'
  gem 'launchy'
  gem 'mime-types'
  gem 'multipart-post'
  gem 'shoulda-matchers'
  gem 'simplecov', require: false
  gem 'timecop'
  gem 'vcr'
  gem 'webmock'
end

gem_group :development, :test do
  gem 'factory_bot_rails'
  gem 'ffaker'
  gem 'pry-rails'
  gem 'rspec-rails'
  gem 'rubocop', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
  gem 'rubocop-performance', require: false
end
