remote_template 'nvmrc.erb', '.nvmrc' if @node_version

inject_into_file 'bin/setup', after: "(\"bundle install\")\n" do
  <<-RUBY
    system("yarn install")\n
  RUBY
end
