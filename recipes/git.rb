# frozen_string_literal: true

git :init
append_to_file '.gitignore', "\nnode_modules/\n"
git remote: "add origin #{@git_repo}"
