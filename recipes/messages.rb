# frozen_string_literal: true

say('=====================================================================')
say('You have installed a new Rails app')
git_url = @git_repo.gsub(':', '/').gsub('git@', 'https://').gsub('.git', '')
say('You can access the repository at:')
say(git_url, :green)
say('')
say('Please follow the instructions below before making your first commit:')
say('')
if @devise
  say('Devise', :yellow)
  say('* change config.mailer_sender in config/initializers/devise.rb')
  say('')
end
unless @setup_database
  say('Database', :yellow)
  say('* run `bin/setup` to set up databases within your devcontainer')
  say('* run `git add . && git commit -am "Initial commit"')
end
say('Deployment', :yellow)
say('* Configure `docker-compose.yml` and `.docker-compose/*.yml` appropriately')
say('')
say('* Add the following global variables to your Gitlab CI:')
say('  RAILS_MASTER_KEY: ', :green)
say(`cat config/master.key`, :yellow)
say('  SENTRY_DSN:       ', :green)
say(@sentry_dsn, :yellow)
say('  PRODUCTION_URL:   ', :green)
say('Ask your server administrator')
say('=====================================================================')
