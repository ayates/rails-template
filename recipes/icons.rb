# frozen_string_literal: true

generate 'heroicon:install'

gsub_file 'app/helpers/heroicon_helper.rb', "# Heroicon Helper Loader\n", before: 'module HeroiconHelper'
