# frozen_string_literal: true

rails_command 'tailwindcss:install'

remove_file 'config/tailwind.config.js'
remove_file 'app/assets/stylesheets/application.tailwind.css'
remote_template 'config/tailwind.config.js'
remote_template 'config/tailwind.mailer.config.js'
remote_template 'lib/tailwindcss/commands_ext.rb'
remote_template 'lib/tasks/tailwind.rake'
remote_template 'app/assets/stylesheets/tailwind.tailwind.css'
remote_template 'app/assets/stylesheets/mailer.tailwind.css'

gsub_file 'app/views/layouts/application.html.erb', 'stylesheet_link_tag "application"',
          'stylesheet_link_tag "tailwind", "application", "inter-font"'

inject_into_file 'app/views/layouts/mailer.html.erb',
                 after: "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" do
  <<-HTML
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <%= stylesheet_link_tag 'mailer', media: 'all' %>
  HTML
end

system 'yarn add tailwindcss-box-shadow tailwindcss-email-variants' # For HTML emails
