# frozen_string_literal: true

RSpec.configure do |c|
  c.before(:each, type: :system) do
    driven_by :rack_test
  end

  c.before(:each, type: :system, js: true) do
    driven_by :cuprite_remote
  end

  c.after(:each, type: :system, js: true) do
    if ENV['CI'] || ENV['REMOTE_CONTAINERS']
      Capybara.reset_sessions!
      Capybara.use_default_driver
      Capybara.app_host = nil
    end
  end
end
