# frozen_string_literal: true

Tailwindcss::Commands.class_eval do
  def self.compile_file_command(file:, debug: false, **kwargs) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    file = Pathname.new(file.to_s)
    input = file
    output = Rails.root.join('app/assets/builds', file.basename.sub('tailwind.', ''))
    config = input.open(&:readline).start_with?('@config') ? nil : Rails.root.join('config/tailwind.config.js')
    [
      executable(**kwargs),
      '-i', input.to_s,
      '-o', output.to_s
    ].tap do |command|
      command << '-c' if config
      command << config.to_s if config
      command << '--minify' unless debug
    end
  end

  def self.watch_file_command(poll: false, **kwargs)
    compile_file_command(**kwargs).tap do |command|
      command << '-w'
      command << '-p' if poll
    end
  end
end
